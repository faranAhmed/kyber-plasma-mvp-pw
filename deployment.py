from plasma.peer_wallets.deployer import Deployer

deployer = Deployer()
deployer.compile_all()
deployer.deploy_contract("PeerWallets")
