pragma solidity ^0.4.0;
import "./PeerWallets.sol";

/**
 * @title PeerWalletsEngine
 * @dev To launch a peer wallet with more than one peers and specified distribution for each group
 */
 contract PeerWalletsEngine{
    
    /**
     * @dev Payable function to launch a new peer wallet after coniditons;
     * - If number of peers for a wallet are greater than 1
     * - If exchange groups are equal to the number of distributions
     * @param _peers array of addresses, peers for wallet
     * @param _exchangeGroups array of addresses, exchange groups (tokens) to invest
     * @param _distribution array of unsigned integer percentage distributions for exchange group
     * @return address of newly created peerWallet successfully, address of account 0 otherwise
     */
     function launchPeerWallet(address[] _peers, address[] _exchangeGroups, uint[] _distribution)
        public
        payable
        returns(address) {
        if(_peers.length > 1 && _exchangeGroups.length == _distribution.length){
            PeerWallets peerWalletsSCObj = new PeerWallets();
            return address(peerWalletsSCObj.createPeerWallet.value(msg.value)(msg.sender, _peers, _exchangeGroups, _distribution));
        }
        return address(0);
    }
}
