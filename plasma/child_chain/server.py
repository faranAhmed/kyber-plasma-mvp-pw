import rlp
from web3 import Web3, WebsocketProvider, HTTPProvider
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher
from ethereum import utils
from plasma.child_chain.PeerWallets import PeerWallets
from plasma.child_chain.child_chain import ChildChain
from plasma_core.constants import ROOT_CHAIN_ABI, ROOT_CHAIN_CONTRACT_ADDRESS, AUTHORITY, PEER_WALLETS_ABI, PEER_WALLETS_CONTRACT_ADDRESS
from plasma_core.block import Block
from plasma_core.transaction import Transaction

root_chain = Web3(WebsocketProvider('wss://ropsten.infura.io/ws')).eth.contract(abi=ROOT_CHAIN_ABI, address=ROOT_CHAIN_CONTRACT_ADDRESS)
child_chain = ChildChain(AUTHORITY['address'], root_chain)

peerWalletsInstance = Web3(HTTPProvider('http://localhost:8545')).eth.contract(abi=PEER_WALLETS_ABI, address=PEER_WALLETS_CONTRACT_ADDRESS)
peerwallets = PeerWallets(AUTHORITY['address'], peerWalletsInstance)


@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}
    dispatcher["submit_block"] = lambda block: child_chain.submit_block(rlp.decode(utils.decode_hex(block), Block))
    dispatcher["apply_transaction"] = lambda transaction: child_chain.apply_transaction(rlp.decode(utils.decode_hex(transaction), Transaction))
    dispatcher["get_transaction"] = lambda blknum, txindex: rlp.encode(child_chain.get_transaction(blknum, txindex), Transaction).hex()
    dispatcher["get_current_block"] = lambda: rlp.encode(child_chain.get_current_block(), Block).hex()
    dispatcher["get_current_block_num"] = lambda: child_chain.get_current_block_num()
    dispatcher["get_block"] = lambda blknum: rlp.encode(child_chain.get_block(blknum), Block).hex()
    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    run_simple('localhost', 8546, application)
