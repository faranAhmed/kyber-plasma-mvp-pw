from web3 import Web3, WebsocketProvider, HTTPProvider
from .peer_wallets_listener import PeerWalletsListener
from plasma_core.constants import PEER_WALLETS_ABI, ROOT_CHAIN_ABI, ROOT_CHAIN_CONTRACT_ADDRESS


class PeerWallets(object):

    def __init__(self, operator, peerwallets):
        self.operator = operator
        self.w3 = Web3(HTTPProvider('http://localhost:8545'))
        self.peerWallets = peerwallets

        # Listen for events
        self.event_listener = PeerWalletsListener(peerwallets, confirmations=0)
        self.event_listener.on('TotalInv', self.apply_deposit_kyber_tokens)

    def apply_deposit_kyber_tokens(self, event):
        event_args = event['args']
        owner = Web3.toChecksumAddress(event_args['owner'])
        totalinvestment = event_args['totalInvested']
        peerwallets_contract_address = Web3.toChecksumAddress(event_args['peerWallet'])
        print(event_args)
        root_chain_instance = Web3(WebsocketProvider('wss://ropsten.infura.io/ws')).eth.contract(abi=ROOT_CHAIN_ABI, address=ROOT_CHAIN_CONTRACT_ADDRESS)
        if totalinvestment > 0:
            peer_wallets_instance = self.w3.eth.contract(address=peerwallets_contract_address, abi=PEER_WALLETS_ABI)
            maxKeys = peer_wallets_instance.functions.getExchangeGroupsLength().call()
            for x in range(0, maxKeys):
                exchange_group_keys = peer_wallets_instance.functions.getExchangeGroups(x).call()
                print(" EXCHANGE GROUP KEY {0}".format(exchange_group_keys))
                root_chain_instance.functions.depositKyberTokens(exchange_group_keys, peerwallets_contract_address, totalinvestment).call({'from': owner})
