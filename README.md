# Kyber Plasma MVP PW (PeerWallets)

We're implementing Minimum Viable Plasma to buy desired tokens.

### root_chain

`root_chain` smart contract is deployed on address : 0x15De19418cb401884421d8D398A0d8503c4ff210

### child_chain

`child_chain` represents our PeerWallets smart contracts while a python implementation of a Plasma client used for transaction transfer from root-chain to child-chain (PeerWallets)

`child_chain` also contains an RPC server that enables client interactions. By default, this server runs on port `8545`.

## Getting Started

### Dependencies

This project has a few pre-installation dependencies.

#### [LevelDB](https://github.com/google/leveldb)

Mac:
```sh
brew install leveldb
```

Linux:

LevelDB should be installed along with `plyvel` once you make the project later on.

Windows:

First, install [vcpkg](https://github.com/Microsoft/vcpkg). Then,

```sh
vcpkg install leveldb
```


#### [Solidity](https://solidity.readthedocs.io/en/latest/installing-solidity.html)

Mac:
```sh
brew update
brew upgrade
brew tap ethereum/ethereum
brew install solidity
```

Linux:
```sh
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc
```

Windows:

Follow [this guide](https://solidity.readthedocs.io/en/latest/installing-solidity.html#prerequisites-windows)


#### [Python 3.2+](https://www.python.org/downloads/)

Mac:
```sh
brew install python
```

Linux:
```sh
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3
```

Windows:
```sh
choco install python
```

#### [Ganache CLI 6.1.8+](https://github.com/trufflesuite/ganache-cli)

### Installation

Note: we optionally recommend using something like [`virtualenv`](https://pypi.python.org/pypi/virtualenv) in order to create an isolated Python environment:

```
$ virtualenv env -p python3
```

Fetch and install the project's dependencies with:

```
$ make
```

### Starting Plasma

The fastest way to start playing with our Plasma MVP is by starting up `ganache-cli`, deploying these statements locally, and running two different CLI.

```bash
$ ganache-cli -m=plasma_mvp # Start ganache-cli on another CLI Window
$ make peer-wallets           # Deploys the peer-wallets contract
$ make child-chain          # Run our child chain and server
```

#### Usage

```
help
```

### `deposit`

#### Description

Deploy Root-Chain in Remix compiler by setting "AT ADDRESS" with contract address defined above. Call function "depositEther". 

#### Usage

```
deposit <address>
```

#### Example

```
deposit 0xA3B2a1804203b75b494028966C0f62e677447A39
```

Now you can see logs/events printed on CLI where you deployed contracts using make command.

It shows a cycle of Root-chain to PeerWallets through python programmed files.
