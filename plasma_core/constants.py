from ethereum import utils as u

PEER_WALLETS_CONTRACT_ADDRESS = '0xA3B2a1804203b75b494028966C0f62e677447A39'
ROOT_CHAIN_CONTRACT_ADDRESS = '0x6892D3A29Db5eBd421D2ad61A0f4BE615Fdd298e'

PEER_WALLETS_ABI = '''[{"constant":false,"inputs":[{"name":"_exchangeGroupKey","type":"address"},{"name":"_tokens","type":"uint256"}],"name":"tradeWalletTokens","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_peers","type":"address[]"}],"name":"addPeers","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"withdrawAllTokens","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_leader","type":"address"},{"name":"_peers","type":"address[]"},{"name":"_exchangeGroupKeys","type":"address[]"},{"name":"_distribution","type":"uint256[]"}],"name":"createPeerWallet","outputs":[{"name":"","type":"address"}],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"_peer","type":"address"}],"name":"getPeerOwnership","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_exchangeGroupKey","type":"address"}],"name":"withdrawTokens","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_peer","type":"address"}],"name":"removePeer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_index","type":"uint256"}],"name":"getExchangeGroupsKeyAt","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_peer","type":"address"}],"name":"validatePeer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"triggerInvestment","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_amount","type":"uint256"}],"name":"makeInvestment","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_index","type":"uint256"}],"name":"getDistributionAt","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getPeers","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getExchangeGroupsLength","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_peer","type":"address"}],"name":"getPeerTokens","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_amount","type":"uint256"}],"name":"launchInvestment","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"completeInvestment","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"totalInvested","type":"uint256"},{"indexed":true,"name":"peerWallet","type":"address"}],"name":"TotalInv","type":"event"}]'''
ROOT_CHAIN_ABI = '''[{"constant":false,"inputs":[{"name":"_utxoPos","type":"uint256"},{"name":"_txBytes","type":"bytes"},{"name":"_proof","type":"bytes"},{"name":"_sigs","type":"bytes"}],"name":"startExit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_cUtxoPos","type":"uint256"},{"name":"_eUtxoIndex","type":"uint256"},{"name":"_txBytes","type":"bytes"},{"name":"_proof","type":"bytes"},{"name":"_sigs","type":"bytes"},{"name":"_confirmationSig","type":"bytes"}],"name":"challengeExit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"exits","outputs":[{"name":"owner","type":"address"},{"name":"token","type":"address"},{"name":"amount","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_token","type":"address"},{"name":"_amount","type":"uint256"}],"name":"startFeeExit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"operator","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"currentChildBlock","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_token","type":"address"}],"name":"finalizeExits","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_blockNumber","type":"uint256"}],"name":"getChildChain","outputs":[{"name":"","type":"bytes32"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_token","type":"address"}],"name":"getNextExit","outputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"destExchangeAddress","type":"address"},{"name":"amount","type":"uint256"},{"name":"peerWallet","type":"address"}],"name":"depositKyberTokens","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"CHILD_BLOCK_INTERVAL","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"currentDepositBlock","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_root","type":"bytes32"}],"name":"submitBlock","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getDepositBlock","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"currentFeeExit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"exitsQueues","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_utxoPos","type":"uint256"}],"name":"getExit","outputs":[{"name":"","type":"address"},{"name":"","type":"address"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"peerWallet","type":"address"}],"name":"deposit","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_depositPos","type":"uint256"},{"name":"_token","type":"address"},{"name":"_amount","type":"uint256"}],"name":"startDepositExit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"childChain","outputs":[{"name":"root","type":"bytes32"},{"name":"timestamp","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"depositor","type":"address"},{"indexed":true,"name":"depositBlock","type":"uint256"},{"indexed":false,"name":"peerWallet","type":"address"},{"indexed":true,"name":"amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"depositor","type":"address"},{"indexed":false,"name":"peerWallet","type":"address"},{"indexed":true,"name":"destExchangeAddress","type":"address"},{"indexed":true,"name":"totalTokens","type":"uint256"}],"name":"DepositKyber","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"exitor","type":"address"},{"indexed":true,"name":"utxoPos","type":"uint256"},{"indexed":false,"name":"token","type":"address"},{"indexed":false,"name":"amount","type":"uint256"}],"name":"ExitStarted","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"root","type":"bytes32"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"BlockSubmitted","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"token","type":"address"}],"name":"TokenAdded","type":"event"}]'''

AUTHORITY = {
    'address': '0xf76B6526f3B71C8ADf0e9CfA8E831c3E61F5Af44',
    'key': u.normalize_key(b'1887A4D5BD6FF7F6683906422628E0A971ACFF4FB8A2EB57E87252BDB2C2161B')
}

ACCOUNTS = [
    {
        'address': '0x4B3eC6c9dC67079E82152d6D55d8dd96a8e6AA26',
        'key': u.normalize_key(b'b937b2c6a606edf1a4d671485f0fa61dcc5102e1ebca392f5a8140b23a8ac04f')
    },
    {
        'address': '0xda20A48913f9031337a5e32325F743e8536860e2',
        'key': u.normalize_key(b'999ba3f77899ba4802af5109221c64a9238a6772718f287a8bd3ca3d1b68187f')
    },
    {
        'address': '0xF6d8982698dCC46b8E96e34bC2BF3c97302b9923',
        'key': u.normalize_key(b'ef4134d11aa32bcbd314d3cd94b7a5f93bea2b809007d4307a4393cce0285652')
    },
    {
        'address': '0x2d75468C0cafA9D41FC5BF3ccA6C292a3cC03d94',
        'key': u.normalize_key(b'25c8620e5bd51caed1d2ff5e79b43dfbef17d0b4eb38d0db8d834da9de5a6120')
    },
    {
        'address': '0xF05B4B746AAd830062505AD0cFd3619917484E46',
        'key': u.normalize_key(b'e3d66a68573a85734e80d3de47b82e13374c2a026f219cb766978510a8b8697e')
    },
    {
        'address': '0x81A9bfA79598f1536B4918A6556e9855c5E141d5',
        'key': u.normalize_key(b'81e244b79cef097c187d9299a2fc3a680cf1d2637fb7463ca7aa70445a0a0410')
    },
    {
        'address': '0xa669513ad878cC0891d8C305CC21903068a9AFe9',
        'key': u.normalize_key(b'ebcaa9c519c2aaa27e7c1656451b9c72167cadf0fd30bc4bcc3bda6d6fcbd507')
    },
    {
        'address': '0xC3AaE3a9be258BD485105ef81EB0D5b677EE26fd',
        'key': u.normalize_key(b'b991543d47829ea4f296d182dfa7088303fb3f04dd0c95a5cb7132397e4a008d')
    },
    {
        'address': '0xB9DB71c2D02a1B30dfE29C90738b3228Dd9d2ec2',
        'key': u.normalize_key(b'484eb2f0465e7357575f05bf5af5e77cb4b678fb774dd127d9d99e3d31c5f80e')
    }
]


NULL_BYTE = b'\x00'
NULL_HASH = NULL_BYTE * 32
NULL_SIGNATURE = NULL_BYTE * 65
NULL_ADDRESS = NULL_BYTE * 20
NULL_ADDRESS_HEX = '0x' + NULL_ADDRESS.hex()
